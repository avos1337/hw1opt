
Design a function ex1(int_seq, subtotal) that:
    – takes as parameters
      a string (int_seq) and a positive integer (subtotal >= 0), and
    – returns the number of substrings of int_seq such that
      the sum of their values is equal to subtotal.

Hint: you can obtain a substring by picking any consecutive
    elements in the original string.

For example, given int_seq = '3,0,4,0,3,1,0,1,0,0,5,0,4,2' and subtotal = 9,
    the function should return 7. The following substrings, indeed, consist of
    values whose sum is equal to 9:
    int_seq = '3,0,4,0,3,1,0,1,0,1,0,0,5,0,4,2'
            => _'0,4,0,3,1,0,1,0'_____________
               _'0,4,0,3,1,0,1'_______________
               ___'4,0,3,1,0,1,0'_____________
               ___'4,0,3,1,0,1'_______________
               ___________________'0,0,5,0,4'_
               _____________________'0,5,0,4'_
               _______________________'5,0,4'_

NOTE: it is FORBIDDEN to use/import any libraries

NOTE: Each test must terminate on the VM before the timeout of
    1 second expires.

WARNING: Make sure that the uploaded file is UTF8-encoded
    (to that end, we recommend you edit the file with Spyder)
'''

Effectivley, the improved program idea should have two pointers (substring START and END) and a running sum.
What the program does should depend on that sum.
Start both pointers at the 0 position.
If the sum is under the subtotal: Move END pointer right.
If the sum is equal to the subtotal: add one to the n_subs and move END pointers to the right.
If the sum is over the subtotal move the START pointer to the right.
Update the running sum after every pointer movement. 
