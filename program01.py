"""Optional HW 1"""


def __sanitze(int_seq: str) -> list:
    """Gets a string turns it into a list of ints"""
    str_lst = int_seq.split(',')  # Split the string into a list
    int_lst = []  # Init empty integer list

    # Transform string list into integer list
    for item in str_lst:
        int_lst.append(int(item))
    return int_lst


def f_end(end, rns, int_lst):
    """Moves end pointer forward"""
    end += 1
    rns += int_lst[end]

    return end, rns


def b_end(end, rns, int_lst):
    """Moves end pointer backwards"""
    rns -= int_lst[end]
    end -= 1

    return end, rns


def f_start(start, rns, int_lst):
    """Moves start pointer forewards"""
    rns -= int_lst[start]
    start += 1

    return start, rns


def b_start(start, rns, int_lst):
    """Moves start pointer backward"""
    start -= 1
    rns += int_lst[start]

    return start, rns


def feel_ahead(pointer, offset, int_lst):
    """Returns the existance of the nth offset element from a pointer"""
    index = pointer + offset
    if index >= 0 and index <= len(int_lst) - 1:
        return True

    return False


def look_ahead(pointer, offset, int_lst):
    """Returns the nth offset element from a pointer"""

    if feel_ahead(pointer, offset, int_lst):
        return int_lst[pointer + offset]

    return False


def ex1(int_seq, subtotal):
    """Main excercise variable"""
    # INIT STUFF
    n_subs = 0
    int_lst = __sanitze(int_seq)  # Sanitize the input into a predicatable integer list # noqa
    lng = len(int_lst)
    rns = 0  # Running sum, keeps track of what the program has between its pointers # noqa
    # Init Pointers
    start = 0  # Start pointer
    end = 0  # End pointer

    # Set rns to 0,0
    rns = int_lst[start-end]

    # While loop

    start_offset = 0

    while start < lng and end < lng:
        if rns < subtotal:

            if feel_ahead(end, 1, int_lst):
                end, rns = f_end(end, rns, int_lst)
            else:
                return n_subs

        elif rns == subtotal:

            if look_ahead(start, start_offset, int_lst) == 0:
                n_subs += 1
                start_offset += 1

            else:

                n_subs += 1
                start_offset = 0
                if feel_ahead(end, 1, int_lst):
                    end, rns = f_end(end, rns, int_lst)
                else:
                    break
        elif rns > subtotal:

            start, rns = f_start(start, rns, int_lst)

    return n_subs


if __name__ == '__main__':
    # Insert your own tests here
    pass
